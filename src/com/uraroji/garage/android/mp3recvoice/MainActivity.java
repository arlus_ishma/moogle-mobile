package com.uraroji.garage.android.mp3recvoice;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.Executors;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONObject;


public class MainActivity extends Activity {
    class UploadFileTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String responseString = "";
            HttpResponse response = null;
            try {
                String url = "http://178.62.209.46:8000/api/tag/";
                File track = new File(Environment.getExternalStorageDirectory(), "mezzo.mp3");
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
                reqEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                FileBody fb = new FileBody(track);
                //InputStream inputStream = new ;
                //reqEntity.addPart("track", fb);
                reqEntity.addBinaryBody("track", track);
                final HttpEntity myEntity = reqEntity.build();
                httppost.setEntity(myEntity);

                response = httpclient.execute(httppost);
                //process response
                responseString = new BasicResponseHandler().handleResponse(response);

            }

            catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally
            {
                if (null!=response)
                {
                    try
                    {
                        HttpEntity httpEntity = response.getEntity();
                        if (null != httpEntity)
                        {
                            httpEntity.consumeContent();
                        }
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            return responseString;

        }


        protected void onPostExecute(final String responseString) {
            final TextView result = (TextView) findViewById(R.id.result);
            runOnUiThread(new Thread() {
                public void run() {
                    Toast.makeText(getApplicationContext(), responseString, Toast.LENGTH_LONG).show();
                    String name = new String();
                    try {
                        JSONObject obj = new JSONObject(responseString);
                        name = obj.getString("track");
                    } catch (Exception e){
                        //oops
                    }
                    result.setText(name);
                }
            });
        }
    }
	private RecMicToMp3 mRecMicToMp3 = new RecMicToMp3(
			Environment.getExternalStorageDirectory() + "/mezzo.mp3", 44100);



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		final TextView statusTextView = (TextView) findViewById(R.id.StatusTextView);

		mRecMicToMp3.setHandle(new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case RecMicToMp3.MSG_REC_STARTED:
					statusTextView.setText("Recording");
					break;
				case RecMicToMp3.MSG_REC_STOPPED:
					statusTextView.setText("");
					break;
				case RecMicToMp3.MSG_ERROR_GET_MIN_BUFFERSIZE:
					statusTextView.setText("");
					Toast.makeText(MainActivity.this,
                            "buffer size error",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_CREATE_FILE:
					statusTextView.setText("");
					Toast.makeText(MainActivity.this, "error creating file",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_REC_START:
					statusTextView.setText("");
					Toast.makeText(MainActivity.this, "error while initializing recording",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_AUDIO_RECORD:
					statusTextView.setText("");
					Toast.makeText(MainActivity.this, "error recording audio",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_AUDIO_ENCODE:
					statusTextView.setText("");
					Toast.makeText(MainActivity.this, "error encoding audio",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_WRITE_FILE:
					statusTextView.setText("");
					Toast.makeText(MainActivity.this, "error writing to file",
							Toast.LENGTH_LONG).show();
					break;
				case RecMicToMp3.MSG_ERROR_CLOSE_FILE:
					statusTextView.setText("");
					Toast.makeText(MainActivity.this, "error closing file",
							Toast.LENGTH_LONG).show();
					break;
				default:
					break;
				}
			}
		});

		ImageButton startButton = (ImageButton) findViewById(R.id.StartButton);
		startButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mRecMicToMp3.start();
                CountDownTimer countDowntimer = new CountDownTimer(15000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }

                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                    public void onFinish() {
                        //UploadFileTask mtask = null;
                        mRecMicToMp3.stop();
                        Toast.makeText(getApplicationContext(), "Stopped recording Automatically ", Toast.LENGTH_LONG).show();
                        //mtask.executeOnExecutor(Executors.newSingleThreadExecutor());
                        UploadFileTask mtask = new UploadFileTask();
                        //mtask.executeOnExecutor(Executors.newSingleThreadExecutor());
                        mtask.execute();
                    }};countDowntimer.start();

            }
		});



	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mRecMicToMp3.stop();
	}
}
